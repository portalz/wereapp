<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\MoonPhase;

class ApiController extends Controller
{
    public function getCurrentMoonData()
    {
        $moon=new MoonPhase();

        $moon_data['illumination']=$moon->get('illumination');
        $moon_data['phase']= $moon->phase();

        return Response()->json($moon_data,200);
    }
}
