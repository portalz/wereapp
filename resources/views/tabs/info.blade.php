<div role="tabpanel" class="tab-pane" id="tab_pane_info">
    <h1 style="font-family:verdana; padding-left:35px">Lycanthropic Basics</h1>
    <div style="padding-left: 20px; padding-right: 20px; padding-bottom: 25px; padding-top: 0px">
        <hr class="hr50">
        <p class="indent">This app was developed to help those afflicted with all forms of Lycanthropy to keep track of
            when transformations will potentially occur and to aide in giving them some control and a chance to live a
            more normal life.</p>
        <p class="indent">That being said, this will only apply to Lycanthropes with the most common, lunar
            transformation variant, if you have a strain that forces changes outside of the lunar cycle please do not
            use this application as a resource.</p>
        <p class="indent">Here is some helpful information and tips for Lycanthropes using this application.</p>
        <p class="indent"><b>Please note, due to their abundance, the term "Lycanthrope" covers all forms of
                Therianthropy, not just werewolves.</b></p>
        <hr class="hr50">
        <b>1. - Be Smart.</b>
        <p class="indent">If the moon is full, don't make plans, commonsense is your best ally here.</p>
        <hr class="hr50">
        <b>2. - As a Lycanthrope, it is impossible to predict when a transformation will occur.</b>
        <p class="indent">On average it is 15 - 30 min after Moonrise, but that is only an average, never a certainty.
            Please always be vigilant and use good judgement when deciding to go out near the full moon.</p>
        <hr class="hr50">
        <b>3. - Even a low chance is still a chance!</b>
        <p class="indent">Every Lycanthrope is different. Some are more, and some are less prone to transformations.
            what one might be a small chance may be a larger chance for others, and vis versa. Get a feel for your curse
            before chancing anything.</p>
        <hr class="hr50">
        <b>4. - Always be prepared! </b>
        <p class="indent">Preparation is a huge part of being a Lycanthrope. If you are chancing being out and about
            when the moon is waxing, always be sure to have an ample amount of food on hand, post-transformation hunger
            frenzy is the leading cause of Lycanthrope attacks Globally.</p>
        <p class="indent">It is also advised to plan ahead and not be driving or planning to be in a confined area
            around the time the transformation may be triggered. This should be obvious, but transforming while driving
            is a good way to kill yourself and others, and transforming in a crowd is not advised.</p>
        <hr class="hr50">
        <b>5. - Lastly, don't let your curse control your life.</b>
        <p class="indent">So you turn into a wolf creature (or other species) a few nights a month. You are still a
            unique and valuable individual. You deserve happiness and to live your life as much as the next person.
            Remember, we love you, and you can learn to handle this.</p>
    </div>
</div>
