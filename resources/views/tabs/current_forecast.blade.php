<div role="tabpanel" class="tab-pane active" id="tab_pane_current_forecast">
    <div class="current_forecast_pane">
        <div class="current_forecast_pane_content">
            <br>
            <p><span class="h1 current-tf-chance">null</span><span class="h1">%</span> chance tonight</p>
            <p><span class="h3 current-percent-illumination">null</span><span class="h3">%</span> illumination </p>

            <div><b>Moon Rise & Set</b>
                <div class="moonrise-status">Location permission required to display moon rise and set times. Tap here
                    to retry.
                </div>
            </div>
        </div>
    </div>
    <p class="text-center">Upcoming Forecast</p>
    <div class="current-10day-forecast">


    </div>


</div>
