<div role="tabpanel" class="tab-pane" id="tab_pane_disclaimer">
    <h1 style="font-family:verdana; padding-left:35px">Disclaimer</h1>
    <div style="padding-left: 20px; padding-right: 20px; padding-bottom: 20px; padding-top: 0px">
        <p class="indent">This Application is meant to be used for reference only. The chances of transformation or
            compiled from averages and not to be used as absolutes. If you are a Lycanthrope, please use good judgement
            and caution when around the full moon. This app and it's developers take no responsibility for any actions
            taken by users of this application.</p>
        <br>
        <hr class="hr50">
        <br>
        <h3><b>Credits</b></h3>
        <p>Developed by <a href="https://alyx.tf">Alyx</a> and <a
                href="https://www.patreon.com/doggirlkari">DogGirlKari</a></p>
        <h3><b>Licenses</b></h3>
        <div class="license-item">
            <p><b>Suncalc</b></p>
            <pre>
                    Copyright (c) 2014, Vladimir Agafonkin
        All rights reserved.

        Redistribution and use in source and binary forms, with or without modification, are
        permitted provided that the following conditions are met:

           1. Redistributions of source code must retain the above copyright notice, this list of
              conditions and the following disclaimer.

           2. Redistributions in binary form must reproduce the above copyright notice, this list
              of conditions and the following disclaimer in the documentation and/or other materials
              provided with the distribution.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
        EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
        MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
        COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
        EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
        SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
        HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
        TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
        </pre>
            <a href="https://github.com/mourner/suncalc">https://github.com/mourner/suncalc</a>
            <br>
            <br>
        </div>
        <hr class="hr50">
        <p style="font-family:verdana; font-size:18px; padding-left:15px; padding-bottom:0px"><b>Actual Disclaimer</b>
        </p>
        This app was made for fun, it is not to be taken seriously, Lycanthropes are not real... or are they?
    </div>
</div>
