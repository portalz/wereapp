<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
    <div class="pull-left">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">LycanthrApp</a>
    </div>
    <div class="collapse navbar-collapse" id="navbarNav">
        <div class="nav flex-column" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link active" data-toggle="pill" href="#tab_pane_current_forecast" role="tab"
               aria-controls="tab_pane_current_forecast" aria-selected="true">Forecast</a>
            <a class="nav-link" data-toggle="pill" href="#tab_pane_info" role="tab" aria-controls="tab_pane_info"
               aria-selected="false">Information</a>
            <a class="nav-link" data-toggle="pill" href="#tab_pane_disclaimer" role="tab"
               aria-controls="tab_pane_disclaimer" aria-selected="false">Disclaimer</a>
        </div>
    </div>
</nav>
