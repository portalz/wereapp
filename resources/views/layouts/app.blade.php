<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta property="og:title" content="LycanthrApp"/>
    <meta property="og:url" content="https://app.werewolf.tf"/>
    <meta property="og:image" content="/favicon-large.png"/>
    <meta property="og:description" content="a handy app for werewolves">
    <link rel="stylesheet" href="/css/bootstrap.dark.css">
    <style>


        @media (max-width: 768px) {
            p.indent {
                text-indent: 50px;
            }

            /*don't show anything to desktop browsers because it's all broken anyway*/
            .hr25 {
                width: 25%;
            }

            .hr50 {
                width: 50%;
                text-align: left;
                margin-left: 0;
            }

            hr {
                background-color: #626a73;
            }

            #desktop-info {
                display: none;
            }

            .navbar-collapse {
                position: absolute;
                top: 54px;
                left: 0;
                padding-left: 15px;
                padding-right: 15px;
                padding-bottom: 15px;
                width: 100%;
            }

            .navbar-collapse.collapsing {
                height: auto;
                -webkit-transition: left 0.15s ease;
                -o-transition: left 0.15s ease;
                -moz-transition: left 0.15s ease;
                transition: left 0.15s ease;
                left: -100%;
            }

            .navbar-collapse.show {
                left: 0;
                -webkit-transition: left 0.15s ease-in;
                -o-transition: left 0.15s ease-in;
                -moz-transition: left 0.15s ease-in;
                transition: left 0.15s ease-in;
            }

            .current_forecast_pane {
                background-image: url("/img/pane_img/night.jpg");
                background-size: 100% auto;


            }

            .current_forecast_pane_content {
                /*margin-top:1em;*/
                margin-left: 1em;

            }

            .navbar-collapse {
                background-color: #1C1F22;
            }


        }

        .app {
            padding-top: 12.5%;
        }
    </style>
    <title>WereApp</title>
</head>
<body>
<h1 id="desktop-info">site is not formatted for use on desktops, try again on a mobile device. desktop support coming
    soon.</h1>

<div class="app">
    <navbar-container>
        @include('layouts.components.navbar')
    </navbar-container>

    <div class="tab-content">
        @include('tabs.current_forecast')
        @include('tabs.disclaimer')
        @include('tabs.info')
    </div>
</div>


<script src="/js/jquery-3.4.1.min.js"></script>
<script src="/js/bootstrap.bundle.min.js"></script>
<script src="/js/moment.js"></script>
<script src="/js/suncalc.js"></script>
<script>
    //run init on load
    $(document).ready(function () {
        init();
        //hide loading spinner and show page
        // $(".page-loading-spinner").hide();
        // $("#tab_pane_current_forecast").show();
    });

    //page init
    function init() {
        console.log("starting init");
        setInterval(updateCurrentDetails, 60000 * 15)
        getLocation();
        updateCurrentDetails();
        generateUpcomingForecast();
        console.log("done");
    }

    //action to force rerun init
    $(".moonrise-status").click(function () {
            init();
        }
    );

    //closes the slide out menu after someone clicks a link
    $(".nav>a").click(function () {
        $(this).parent().parent().collapse('hide');
    });

    //Update current times on page
    function updateCurrentTime() {
        var now = moment();
        $(".current-time").html(now.format('HH:mm:ss'));

    }

    //Calculates the TF chance based on a formula extrapolated from Kari chart. Returns a decimal rounded to one decimal point.
    function calculateTFchance(illum, phase) {
        x = illum
        if (x < 90)
            return 0
        if (x > 96.5)
            return 100
        if (phase < .50) { //waxing
            chance = ((66113 / 1386000) * Math.pow(x, 7)) - ((12369101 / 396000) * Math.pow(x, 6)) + ((3470718931 / 396000) * Math.pow(x, 5)) - ((54096258077 / 39600) * Math.pow(x, 4)) + ((50583099341617 / 396000) * Math.pow(x, 3)) - ((2837489634465269 / 396000) * Math.pow(x, 2)) + ((103151848031028233 / 462000) * x) - (1639670912776659 / 550)
        } else { //waning
            chance = -((182519 / 9009000) * Math.pow(x, 6)) + ((2545933 / 231000) * Math.pow(x, 5)) - ((89951165063 / 36036000) * Math.pow(x, 4)) + ((1809332986969 / 6006000) * Math.pow(x, 3)) - ((736324210563701 / 36036000) * Math.pow(x, 2)) + ((1478400090702891 / 2002000) * x) - (3055090899424 / 275)
        }
        if (chance < 0)
            return 0
        if (chance > 100)
            return 100
        return chance.toFixed(1);
    }

    function updateCurrentDetails() {
        currentMoonRiseSetTimes = getFutureMoonRiseSetTimes(moment());
        if (currentMoonRiseSetTimes.length == 1) {
            moon = SunCalc.getMoonIllumination(getPseudoMeridianPeak(currentMoonRiseSetTimes[0]['rise'], currentMoonRiseSetTimes[0]['set']))
        } else if (currentMoonRiseSetTimes.length == 2) {
            //first one has not yet passed
            if (currentMoonRiseSetTimes[0]['set'] > moment()) {
                moon = SunCalc.getMoonIllumination(getPseudoMeridianPeak(currentMoonRiseSetTimes[0]['rise'], currentMoonRiseSetTimes[0]['set']))

            } else {
                moon = SunCalc.getMoonIllumination(getPseudoMeridianPeak(currentMoonRiseSetTimes[1]['rise'], currentMoonRiseSetTimes[1]['set']))
            }
        } else {
            moon = SunCalc.getMoonIllumination(moment())
        }
        $(".current-percent-illumination").html((moon.fraction * 100).toFixed(1));
        $(".current-tf-chance").html(calculateTFchance((moon.fraction * 100).toFixed(1), moon.phase));
        $(".moonrise-status").html("");
        iteration = 0;
        if (currentMoonRiseSetTimes != false) {
            currentMoonRiseSetTimes.forEach(function () {
                $(".moonrise-status").append('<p>' + moment(currentMoonRiseSetTimes[iteration]['rise']).format("HH:mm:ss") + ' – ' + moment(currentMoonRiseSetTimes[iteration]['set']).format("HH:mm:ss"));
                iteration++;
            });
        } else {
            $(".moonrise-status").append('<p>There was an error.</p>');
        }
    }

    //Gets the timestamp of the guesstimated meridian passing time, in our case we will guess it to be in the middle of the time between rise and set.
    //Returns a timestamp of the midpoint between the two times.
    function getPseudoMeridianPeak(startTime, endTime) {
        //we're going to guess the meridian passing as being halfway between rise and set
        var meridian = new Date((startTime.getTime() + endTime.getTime()) / 2);

        return meridian;

    }

    function generateUpcomingForecast() {
        for (i = 1; i < 30; i++) {
            var workingDate = moment();
            workingDate.add(i, 'days').calendar();
            // moon = SunCalc.getMoonIllumination(workingDate)
            iteration = 0;
            currentMoonRiseSetTimes = getFutureMoonRiseSetTimes(workingDate);
            if (currentMoonRiseSetTimes.length == 1) {
                moon = SunCalc.getMoonIllumination(getPseudoMeridianPeak(currentMoonRiseSetTimes[0]['rise'], currentMoonRiseSetTimes[0]['set']))
            } else if (currentMoonRiseSetTimes.length == 2) {
                //first one has not yet passed
                if (currentMoonRiseSetTimes[0]['set'] > moment()) {
                    moon = SunCalc.getMoonIllumination(getPseudoMeridianPeak(currentMoonRiseSetTimes[0]['rise'], currentMoonRiseSetTimes[0]['set']))

                } else {
                    moon = SunCalc.getMoonIllumination(getPseudoMeridianPeak(currentMoonRiseSetTimes[1]['rise'], currentMoonRiseSetTimes[1]['set']))
                }
            } else {
                moon = SunCalc.getMoonIllumination(moment())
            }
            forecast_times = "";
            if (currentMoonRiseSetTimes != false) {
                if (currentMoonRiseSetTimes.length == 2) {
                    forecast_times = forecast_times + '<p>(risen from previous day) –' + moment(currentMoonRiseSetTimes[0]['set']).format("HH:mm:ss") + '<br>' + moment(currentMoonRiseSetTimes[iteration]['rise']).format("HH:mm:ss") + '– (sets tomorrow)</p>';
                }
            } else if (SunCalc.getMoonTimes(workingDate).alwaysDown == true) {
                forecast_times = '<p>The sun never rises on this day.</p>'
            }


            $(".current-10day-forecast").append('<div class="card">\n' +
                '            <div class="card-body">\n' +
                '                <h5 class="card-title">' + moment(workingDate).format("MMM DD") + '</h5>\n' +
                '                <h6 class="card-subtitle mb-2 text-muted">' + ((moon.fraction) * 100).toFixed(1) + '% illumination, ' + calculateTFchance((moon.fraction * 100)) + '% chance of TF</h6>\n' +
                '                <p class="card-text">' + forecast_times + '</p>\n' +
                '            </div>\n' +
                '        </div>');
        }
    }


    // Gets rise and set times for the current day.
    // Returns false if the moon is up all day or down all day. Otherwise, it will return an array of arrays of times.
    function getFutureMoonRiseSetTimes(targetDate) {
        //get the rise and set times for the target date
        pretargetDateTimes = SunCalc.getMoonTimes(moment(targetDate).add(-1, 'days'), localStorage.getItem('geopos_lat'), localStorage.getItem('geopos_lon'));
        targetDateTimes = SunCalc.getMoonTimes(moment(targetDate), localStorage.getItem('geopos_lat'), localStorage.getItem('geopos_lon'));
        posttargetDateTimes = SunCalc.getMoonTimes(moment(targetDate).add(1, 'days'), localStorage.getItem('geopos_lat'), localStorage.getItem('geopos_lon'));
        var cycles = [];

        if ('rise' in targetDateTimes && 'set' in targetDateTimes && targetDateTimes.set > targetDateTimes.rise) {
            cycles[0] = [];
            cycles[0]['rise'] = targetDateTimes.rise;
            cycles[0]['set'] = targetDateTimes.set;
        } else if ('rise' in targetDateTimes && 'set' in targetDateTimes && targetDateTimes.set < targetDateTimes.rise) {
            cycles[0] = [];
            cycles[1] = [];
            cycles[0]['rise'] = pretargetDateTimes.rise;
            cycles[0]['set'] = targetDateTimes.set;
            cycles[1]['rise'] = targetDateTimes.rise;
            cycles[1]['set'] = posttargetDateTimes.set;
        } else {
            return false;
        }
        return cycles;

    }

    //Sets geolocation position in local storage
    function setPosition(position) {
        localStorage.setItem('geopos_lat', position.coords.latitude);
        localStorage.setItem('geopos_lon', position.coords.longitude);

    }

    //Gets geolocation using javascript API and passes it to set local function
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(setPosition);
        } else {
            console.log("Geolocation is not supported by this browser.");
        }
    }


</script>
</body>
</html>
